/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putendl_fd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sessaidi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 15:53:55 by sessaidi          #+#    #+#             */
/*   Updated: 2016/01/26 15:53:56 by sessaidi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libio.h"

int	putendl_fd(char const *s, int fd)
{
	int	i;

	if (!s)
		return (write(fd, "(null)\n", 7));
	i = -1;
	while (s[++i])
		write(fd, &s[i], 1);
	i += write(fd, "\n", 1);
	return (i);
}
