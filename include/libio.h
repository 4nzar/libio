/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libio.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sessaidi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/22 17:19:11 by sessaidi          #+#    #+#             */
/*   Updated: 2016/01/26 17:32:48 by sessaidi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBIO_H
# define LIBIO_H

# include <unistd.h>
# include <stdlib.h>
# include <wchar.h>

# define BIN    "01"
# define TRI    "012"
# define QUA    "0123"
# define QUI    "01234"
# define SEN    "012345"
# define SEP    "0123456"
# define OCT    "01234567"
# define NON    "012345678"
# define DEC    "0123456789"
# define HEX_L  "0123456789abcdef"
# define HEX_U  "0123456789ABCDEF"
# define BUF_SIZE 1024

int		putchar(int c);
int		putwchar(int c);
int		putstr(char const *s);
int		putwstr(wchar_t const *s);
int		putendl(char const *s);
int		putwendl(wchar_t const *s);
int		putchar_fd(char c, int fd);
int		putwchar_fd(wchar_t c, int fd);
int		putstr_fd(char const *s, int fd);
int		putwstr_fd(wchar_t const *s, int fd);
int		putendl_fd(char const *s, int fd);
int		putwendl_fd(wchar_t const *s, int fd);
void	putnbr(int n);
void	putnbr_fd(int n, int fd);
int		putnbr_base(unsigned long long int nb, char *tab_base);
int		putnbr_base_fd(unsigned long long int nb, char *tab_base, int fd);
int		puts(const char *s);
int		getchar(void);
char	*gets(char *str);
char	*sgets(char *str, int size, int fd);
void	*initialize(void *b, size_t len);
size_t	getlength(const char *str);

#endif
