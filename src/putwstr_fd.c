/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putwstr_fd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sessaidi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 16:40:11 by sessaidi          #+#    #+#             */
/*   Updated: 2016/01/26 16:40:12 by sessaidi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libio.h"

int	putwstr_fd(wchar_t const *s, int fd)
{
	int	i;

	if (!s)
		return (write(fd, "(null)", 6));
	i = -1;
	while (s[++i])
		putwchar_fd(s[i], fd);
	return (i);
}
