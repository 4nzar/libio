/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putwstr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sessaidi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 16:40:06 by sessaidi          #+#    #+#             */
/*   Updated: 2016/01/26 16:40:07 by sessaidi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libio.h"

int	putwstr(wchar_t const *str)
{
	int	i;

	if (!str)
		return (write(2, "(null)", 6));
	i = -1;
	while (str[++i])
		putwchar(str[i]);
	return (i);
}
