/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putchar_fd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sessaidi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 16:38:20 by sessaidi          #+#    #+#             */
/*   Updated: 2016/01/26 16:38:21 by sessaidi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libio.h"

int	putchar_fd(char c, int fd)
{
	return (write(fd, (char*)&c, 1));
}
