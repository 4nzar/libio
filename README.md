# LIBIO - standard input/output library functions

[checkbox]: <>✅🔲

[progression]: <>🔵⚪️

## OUTPUT
- [x] putchar
- [x] putwchar
- [x] putstr
- [x] putendl
- [x] putnbr
- [x] putstr_fd
- [x] putendl_fd
- [x] putnbr

## INPUT
- [x] getchar
- [ ] getwchar
- [ ] getstr
- [ ] getnbr

## TDD


| status| function| progression|
|:----:|:----:|:----:|
| 🔲 | putchar |⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️ |
| 🔲 | putwchar |⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️ |
| 🔲 | putstr |⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️ |
| 🔲 | putendl |⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️ |
| 🔲 | putnbr |⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️ |
| 🔲 | putstr_fd |⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️ |
| 🔲 | putendl_fd |⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️ |
| 🔲 | putstr_fd |⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️ |
| 🔲 | getchar |⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️ |
| 🔲 | getwchar |⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️ |
| 🔲 | getstr |⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️ |
| 🔲 | getnbr |⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️⚪️ |



