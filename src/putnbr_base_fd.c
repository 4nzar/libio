/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putnbr_base_fd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sessaidi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 16:39:25 by sessaidi          #+#    #+#             */
/*   Updated: 2016/01/26 16:39:26 by sessaidi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libio.h"

int	putnbr_base_fd(unsigned long long int nb, char *tab_base, int fd)
{
	char					*result;
	size_t					base;
	int						i;
	unsigned long long int	n;
	size_t					len;

	if (!nb || !tab_base)
		return (write(fd, "0", 1));
	n = nb;
	if (!(result = (char*)malloc(sizeof(char) * 70)))
		return (0);
	initialize(result, sizeof(char) * 70);
	base = getlength(tab_base);
	i = 0;
	while (n >= (unsigned long long int)base)
	{
		result[i++] = (char)tab_base[n % base];
		n = n / base;
	}
	result[i++] = (char)tab_base[n];
	while (--i >= 0)
		write(fd, &result[i], 1);
	len = getlength(result);
	free(result);
	return (len);
}
