/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putwchar_fd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sessaidi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 16:41:00 by sessaidi          #+#    #+#             */
/*   Updated: 2016/01/26 16:41:02 by sessaidi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libio.h"

unsigned int g_mask0 = 0;
unsigned int g_mask1 = 49280;
unsigned int g_mask2 = 14712960;
unsigned int g_mask3 = 4034953344;

static int	size(unsigned long long int n, char *b)
{
	size_t	base;
	char	*result;
	int		i;

	result = 0;
	base = getlength(b);
	if (n == 0)
		write(1, "0", 1);
	else
	{
		if (!(result = (char*)malloc(sizeof(char) * 70)))
			return (0);
		initialize(result, sizeof(char) * 70);
		base = getlength(b);
		i = 0;
		while (n >= base)
		{
			result[i++] = (char)b[n % base];
			n = n / base;
		}
		result[i++] = (char)b[n];
	}
	i = getlength(result);
	free(result);
	return (i);
}

static int	print_second_parts(unsigned int c, unsigned char octet, int fd)
{
	unsigned char	o2;
	unsigned char	o1;

	o2 = (c << 26) >> 26;
	o1 = ((c >> 6) << 27) >> 27;
	octet = (g_mask1 >> 8) | o1;
	write(fd, &octet, 1);
	octet = ((g_mask1 << 24) >> 24) | o2;
	write(fd, &octet, 1);
	return (1);
}

static int	print_third_parts(unsigned int c, unsigned char octet, int fd)
{
	unsigned char	o3;
	unsigned char	o2;
	unsigned char	o1;

	o3 = (c << 26) >> 26;
	o2 = ((c >> 6) << 26) >> 26;
	o1 = ((c >> 12) << 28) >> 28;
	octet = (g_mask2 >> 16) | o1;
	write(fd, &octet, 1);
	octet = ((g_mask2 << 16) >> 24) | o2;
	write(fd, &octet, 1);
	octet = ((g_mask2 << 24) >> 24) | o3;
	write(fd, &octet, 1);
	return (1);
}

static int	print_fourth_parts(unsigned int c, unsigned char octet, int fd)
{
	unsigned char	o4;
	unsigned char	o3;
	unsigned char	o2;
	unsigned char	o1;

	o4 = (c << 26) >> 26;
	o3 = ((c >> 6) << 26) >> 26;
	o2 = ((c >> 12) << 26) >> 26;
	o1 = ((c >> 18) << 29) >> 29;
	octet = (g_mask3 >> 24) | o1;
	write(fd, &octet, 1);
	octet = ((g_mask3 << 8) >> 24) | o2;
	write(fd, &octet, 1);
	octet = ((g_mask3 << 16) >> 24) | o3;
	write(fd, &octet, 1);
	octet = ((g_mask3 << 24) >> 24) | o4;
	write(fd, &octet, 1);
	return (1);
}

int			putwchar_fd(wchar_t c, int fd)
{
	unsigned int	v_c;
	int				siz;
	unsigned char	octet;

	if (!c)
		return (0);
	v_c = c;
	siz = size(c, "01");
	octet = 0;
	if (siz <= 7)
	{
		octet = c;
		write(1, &octet, 1);
	}
	else if (siz <= 11)
		print_second_parts(v_c, octet, fd);
	else if (siz <= 16)
		print_third_parts(v_c, octet, fd);
	else
		print_fourth_parts(v_c, octet, fd);
	return (1);
}
