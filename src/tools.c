/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sessaidi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 16:40:36 by sessaidi          #+#    #+#             */
/*   Updated: 2016/01/26 16:40:37 by sessaidi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libio.h"

void	*initialize(void *b, size_t len)
{
	char	*v_b;

	if (len == 0)
		return (b);
	v_b = (char*)b;
	if (!b)
		return (0);
	while (len-- > 0)
		v_b[len] = 0;
	return (b);
}

size_t	getlength(const char *str)
{
	size_t	len;

	if (!str)
		return (0);
	len = 0;
	while (str[len])
		len++;
	return (len);
}
