/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gets.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sessaidi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/26 15:38:12 by sessaidi          #+#    #+#             */
/*   Updated: 2016/01/26 15:38:15 by sessaidi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libio.h"

static char	*addchar(char *s, char c)
{
	int	i;

	i = 0;
	if (!s)
		return (0);
	while (s[i])
		i++;
	s[i] = c;
	s[i + 1] = 0;
	return (s);
}

char		*gets(char *str)
{
	char	c;

	initialize(str, BUF_SIZE);
	while ((c = getchar()) > 0)
	{
		if (c == '\n')
			break ;
		addchar(str, c);
	}
	return (str);
}
