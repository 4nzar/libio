NAME    =   libio.a

CC		=	clang
FLAGS	=	-Wall -Werror -Wextra

INC	=	include/
SRC	=	src
O_DIR = obj/

C_FILES	= $(shell find $(SRC) -type f | grep \.c$)
O_FILES	= $(patsubst $(SRC)%.c, $(O_DIR)%.o, $(C_FILES))

.PHONY	=	all clean fclean re

all: $(O_FILES)
	@ar rc $(NAME) $(O_FILES)
	@ranlib	$(NAME)
	@echo "🌀 $(NAME) was successfully created!"
	@echo "🌀 type [\033[1;36mmake test\033[0m] to run tests"
	@echo "🌀 type [\033[1;36mmake lint\033[0m] to run linter"

$(O_DIR)%.o: $(SRC)%.c
	@mkdir -p $(dir $@)
	@$(CC) $(FLAGS) -I$(INC) -c $< -o $@ && \
	echo " • build $$(echo $@ | cut -d'/' -f3 | cut -d'.' -f1) function -- \033[0;32mSUCCESS\033[0m"

clean       :
			@/bin/rm -rf $(O_DIR)

fclean      :	clean
			@/bin/rm -rf $(NAME)

lint        : 
			@echo "No linter defined"

test		: 
			@echo "No Test defined"


re          :	fclean all

deploy      : test

